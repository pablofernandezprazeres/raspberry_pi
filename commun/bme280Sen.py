# =============================================================================
# start of bme280Sen.py
# =============================================================================
__author__ = "Pablo Fernandez Prazeres"
__license__ = "GPL"
__version__ = "0.1.3"
__history__ = "none"
__email__ = "pablofernandezprazeres@gmail.com"
__date__ = "12.jul.2023"
# =============================================================================

import smbus2
import bme280
from commun.gpio import I2C_port, I2C_address

# =============================================================================
# These are the global varibles

I2C_bus = 0
I2C_calibration_params = 0
load = 1

# -----------------------------------------------------------------------------
# Connecting the program to the device and we are calibrating it

def init():
    global I2C_bus
    I2C_bus = smbus2.SMBus(I2C_port)

    global I2C_calibration_params
    I2C_calibration_params = bme280.load_calibration_params(I2C_bus, I2C_address)
    print("")
    print("bme280 activated")
    print("")
    
# -----------------------------------------------------------------------------
# Here we are using the previous parameters to create organised data

def read():
    global load
    data = bme280.sample(I2C_bus, I2C_address, I2C_calibration_params)
    print("reading data ", load, "/10")
    load = load + 1
    return data

# -----------------------------------------------------------------------------
# Stop reading data

def close(): 
    print("")
    print("bme280 closed") 
    print("")

# =============================================================================
# end of bme280Sen.py
# =============================================================================
