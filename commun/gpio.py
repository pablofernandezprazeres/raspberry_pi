# =============================================================================
# start of file gpio.py
# =============================================================================
__author__ = "Pablo Fernandez Prazeres"
__license__ = "GPL"
__version__ = "0.1.3"
__history__ = "none"
__email__ = "pablofernandezprazeres@gmail.com"
__date__ = "12.jul.2023"
# =============================================================================

# Physical layout
# ----------------------------------------------------------------------------
# 
GPIO_BASE_ADRESS = 0x487876 # fixme: complete

GPIO_PIN_3 = 3 
GPIO_NAME_3 = "GPIO02" 

GPIO_PIN_5 = 5 
GPIO_NAME_5 = "GPIO03"

# ----------------------------------------------------------------------------

GPIO_PIN_19 = 19
GPIO_NAME_19 = "GPIO10"

GPIO_PIN_21 = 21 
GPIO_NAME_21 = "GPIO09"

GPIO_PIN_23 = 23 
GPIO_NAME_23 = "GPIO11"

# ----------------------------------------------------------------------------

GPIO_PIN_8 = 8 
GPIO_NAME_8 = "GPIO14"

GPIO_PIN_10 = 10
GPIO_NAME_10 = "GPIO15"

# ----------------------------------------------------------------------------
# i2c bus information

I2C_SDA1 = GPIO_PIN_3
I2C_SDL1 = GPIO_PIN_5

I2C_port = 1
I2C_address = 0x76

# ----------------------------------------------------------------------------
# SPI bus information

SPI_SPI_MOSI = GPIO_PIN_19
SPI_SPI_MISO = GPIO_PIN_21
SPI_SPI_CLK = GPIO_PIN_23

# ----------------------------------------------------------------------------
# UART bus information

UART_TXD0 = GPIO_PIN_8
UART_RXD0 = GPIO_PIN_10

# =============================================================================
# end of file gpio.py
# =============================================================================
