# =============================================================================
# start of gpsblux.py
# =============================================================================
__author__ = "Pablo Fernandez Prazeres"
__license__ = "GPL"
__version__ = "0.1.3"
__history__ = "none"
__email__ = "pablofernandezprazeres@gmail.com"
__date__ = "12.jul.2023"
# =============================================================================

from gps import *

# =============================================================================
# global variable

gpsd = gps(mode=WATCH_ENABLE|WATCH_NEWSTYLE) 

# ----------------------------------------------------------------------------

def init():
    print("gps activated")
    print("")
    
# ----------------------------------------------------------------------------
""" here we need the 'while' because we don't always have all the information 
    in one go so it has to read another time the gps, when we have in the 
    report the line 'class' it means that we have all the info needed and can 
    continue
"""

def read():
    report = gpsd.next()
    while report['class'] != 'TPV':
        report = gpsd.next()
        if report['class'] == 'TPV':
            return report
        
# ----------------------------------------------------------------------------
        
def close():
    print("gps closed")
    print("")
        
# =============================================================================
# end of gpsblux.py
# =============================================================================

