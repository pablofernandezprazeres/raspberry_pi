# =============================================================================
# start of main.py
# =============================================================================
__author__ = "Pablo Fernandez Prazeres"
__license__ = "GPL"
__version__ = "0.1.3"
__history__ = "none"
__email__ = "pablofernandezprazeres@gmail.com"
__date__ = "12.jul.2023"
# =============================================================================

import statistics
import time
import commun.bme280Sen as bme280Sen
from commun.bme280Sen import init, read, close
import commun.gpsblux as gpsblux
from commun.gpsblux import init, read, close
import argparse

# =============================================================================

temp = []
pres = []
humi = []
report = 0

# -----------------------------------------------------------------------------

def calculate():
    bme280Sen.init()
    gpsblux.init()
    
    i = 0
    # We read every second the data from the device for 10 seconds
    while(i < 10):
        data = bme280Sen.read()
        temp.append(data.temperature)
        pres.append(data.pressure)
        humi.append(data.humidity)
        i += 1
        time.sleep(1)
    
    global report
    report = gpsblux.read()

    bme280Sen.close()
    gpsblux.close()
    

# -----------------------------------------------------------------------------

def showResults():
    # Calculating average
    MoyTemp = sum(temp) / len(temp)
    MoyTemp = round(MoyTemp, 3)
    MoyPres = sum(pres) / len(pres)
    MoyPres = round(MoyPres, 3)
    MoyHumi = sum(humi) / len(humi)
    MoyHumi = round(MoyHumi, 3)
    # Calculating median
    MedTemp = statistics.median(temp)
    MedTemp = round(MedTemp, 3)
    MedPres = statistics.median(pres)
    MedPres = round(MedPres, 3)
    MedHumi = statistics.median(humi)
    MedHumi = round(MedHumi, 3)
    # Calculating standard deviation
    DevTemp = statistics.stdev(temp)
    DevTemp = round(DevTemp, 3)
    DevPres = statistics.stdev(pres)
    DevPres = round(DevPres, 3)
    DevHumi = statistics.stdev(humi)
    DevHumi = round(DevHumi, 3)
    # taking the info needed from report (gps) 
    tim = report.time
    lat = report.lat
    lat = round(lat, 3)
    lon = report.lon
    lon = round(lon, 3)
    alt = report.alt
    alt = round(alt, 3)
    # printing results
    print("Average temperature :", MoyTemp, "C")
    print("Average pressure :", MoyPres, "Pa")
    print("Average Humidity :",MoyHumi, "%")
    print("")
    print("Median temperature :", MedTemp, "C")
    print("Median pressure :", MedPres, "Pa")
    print("Median Humidity :",MedHumi, "%")
    print("")
    print("Standard Deviation of temperature :", DevTemp)
    print("Standard Deviation of pressure :", DevPres)
    print("Standard Deviation of Humidity :",DevHumi)
    print("")
    print("Time :", tim)
    print("Latitude :", lat, "N")
    print("Longitude :", lon, "E")
    print("Altitude :", alt, "m")
    print("")


    
# -----------------------------------------------------------------------------
# We run the functions

def main():
    calculate()
    showResults()

if __name__ == "__main__":
         main()



# =============================================================================
# end of main.py
# =============================================================================